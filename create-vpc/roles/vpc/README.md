VPC Ansible Role
=========

This role creates a Cloudformation Template from a Jinga2 template and local variables.

The template is rendered and stored in a local folder ../rendered_templates from where it is 
executed against an AWS account using boto.

It create a VPC with a given cidr {{ vpc.cidr }} and associated IGW with a given region
{{ region }}

Requirements
------------

Ansible 1.3 and above

Role Variables
--------------

It currently reads from a global variable file as the playbook can be 
client specific.

It requires:
region
stack_prefix

Dependencies
------------

The role depends on common.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - name: Create VPC and associated network componets
      hosts: localhost
      roles:
        - common
        - vpc
        - network
