Network Ansible Role
=========

This role creates a Cloudformation Template from a Jinga2 template and local variables.

The template is rendered and stored in a local folder ../rendered_templates from where it is 
executed against an AWS account using boto

It creates subnets, routing tables and associations based on a set of simple global variables:


Requirements
------------

Ansible 1.3 and above

Role Variables
--------------

It currently reads from a global variable file as the playbook can be 
client specific.

It requires:

vpc.slash: 
segments.
  Public:
    azs: 2      # number of AZ subnets
    slash: 26   # size of AZ subnets
  Private:
    azs: 2      # number of AZ subnets
    slash: 26   # size of AZ subnets

Dependencies
------------

The role depends on common.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - name: Create VPC and associated network componets
      hosts: localhost
      roles:
        - common
        - vpc
        - network
