Common Ansible Role
=========

This role is high level and is only called if extra tools are needed to 
be installed.

This role will install using yum and pip

Requirements
------------

Ansible 1.3 and above

Role Variables
--------------

It currently reads from a local var file:

tools:
  - libselinux-python
  - git
  
pip_tools:
  - ansible
  - boto3
  - netaddr

Dependencies
------------

The role does require pip to be installed on the system that it is run from.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - name: Create VPC and associated network componets
      hosts: localhost
      roles:
        - common
        - vpc
        - network
